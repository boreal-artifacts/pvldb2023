#!/bin/bash
# This script construct the .json data file we used in our experiments.

[ -z ${year:+x}] &&
year="2022"

[ -z ${month:+x}] &&
month="02"

[ -z ${day:+x}] &&
days=$(seq -f '%02.0f' 1 8)

[ -z ${hours:+x}] &&
#hours=$(seq 0 23)
hours="15"

dir=tmp
finalFile="data.json"
[ -f $finalFile ] && rm $finalFile

for d in $days ; do
for h in $hours ; do
	id="$year-$month-$d-$h"
	jsfile="$id.json"
	gzfile="$jsfile.gz"
	
	[ ! -d "$dir" ] && mkdir "$dir"
	
	
	cd "$dir"
	
	if [ ! -f "$jsfile" ]; then
		url="https://data.gharchive.org/$gzfile"
		echo "downloading $url"
		wget "$url"
		gunzip "$gzfile"
	fi
	
	cat "$jsfile" >> ../$finalFile
	cd ..
done
done
