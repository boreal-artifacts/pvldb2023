FOR D in `tf_DBLP_DBLP-simplified-`
LET b0=(
	FILTER HAS(D["dblp"]["book"],"author")
	RETURN 1
)[0]
LET b1=b0 || (
	FILTER b0 || HAS(D["dblp"]["book"],"journal")
	RETURN 1
)[0]
LET b2=b1 || (
	FILTER b1 || HAS(D["dblp"]["book"],"editor")
	RETURN 1
)[0]
LET b3=b2 || (
	FILTER b2 || HAS(D["dblp"]["book"],"creator")
	RETURN 1
)[0]
LET b4=b3 || (
	FILTER b3 || HAS(D["dblp"]["book"],"entityProperty")
	RETURN 1
)[0]
LET b5=b4 || (
	FILTER b4 || HAS(D["dblp"]["book"],"cite")
	RETURN 1
)[0]
LET b6=b5 || (
	FILTER b5 || HAS(D["dblp"]["book"],"school")
	RETURN 1
)[0]
LET b7=b6 || (
	FILTER b6 || HAS(D["dblp"]["book"],"publisher")
	RETURN 1
)[0]
LET b8=b7 || (
	FILTER b7 || (
		let d0=b7 ? null : D["dblp"]["book"]["author"]
		FILTER IS_ARRAY(d0)
		FOR D1 in d0
		FILTER HAS(D1,"#text")
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b9=b8 || (
	FILTER b8 || (
		let d0=b8 ? null : D["dblp"]["book"]["editor"]
		FILTER IS_ARRAY(d0)
		FOR D1 in d0
		FILTER HAS(D1,"#text")
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b10=b9 || (
	FILTER b9 || HAS(D["dblp"]["book"]["publisher"],"#text")
	RETURN 1
)[0]
LET b11=b10 || (
	FILTER b10 || (
		let d0=b10 ? null : D["dblp"]["book"]["cite"]
		FILTER IS_ARRAY(d0)
		FOR D1 in d0
		FILTER HAS(D1,"#text")
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b12=b11 || (
	FILTER b11 || HAS(D["dblp"]["proceedings"],"author")
	RETURN 1
)[0]
LET b13=b12 || (
	FILTER b12 || HAS(D["dblp"]["proceedings"],"journal")
	RETURN 1
)[0]
LET b14=b13 || (
	FILTER b13 || HAS(D["dblp"]["proceedings"],"editor")
	RETURN 1
)[0]
LET b15=b14 || (
	FILTER b14 || HAS(D["dblp"]["proceedings"],"creator")
	RETURN 1
)[0]
LET b16=b15 || (
	FILTER b15 || HAS(D["dblp"]["proceedings"],"entityProperty")
	RETURN 1
)[0]
LET b17=b16 || (
	FILTER b16 || HAS(D["dblp"]["proceedings"],"cite")
	RETURN 1
)[0]
LET b18=b17 || (
	FILTER b17 || HAS(D["dblp"]["proceedings"],"school")
	RETURN 1
)[0]
LET b19=b18 || (
	FILTER b18 || HAS(D["dblp"]["proceedings"],"publisher")
	RETURN 1
)[0]
LET b20=b19 || (
	FILTER b19 || (
		let d0=b19 ? null : D["dblp"]["proceedings"]["author"]
		FILTER IS_ARRAY(d0)
		FOR D1 in d0
		FILTER HAS(D1,"#text")
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b21=b20 || (
	FILTER b20 || (
		let d0=b20 ? null : D["dblp"]["proceedings"]["editor"]
		FILTER IS_ARRAY(d0)
		FOR D1 in d0
		FILTER HAS(D1,"#text")
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b22=b21 || (
	FILTER b21 || HAS(D["dblp"]["proceedings"]["publisher"],"#text")
	RETURN 1
)[0]
LET b23=b22 || (
	FILTER b22 || (
		let d0=b22 ? null : D["dblp"]["proceedings"]["cite"]
		FILTER IS_ARRAY(d0)
		FOR D1 in d0
		FILTER HAS(D1,"#text")
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b24=b23 || (
	FILTER b23 || HAS(D["dblp"]["thesis"],"author")
	RETURN 1
)[0]
LET b25=b24 || (
	FILTER b24 || HAS(D["dblp"]["thesis"],"journal")
	RETURN 1
)[0]
LET b26=b25 || (
	FILTER b25 || HAS(D["dblp"]["thesis"],"editor")
	RETURN 1
)[0]
LET b27=b26 || (
	FILTER b26 || HAS(D["dblp"]["thesis"],"creator")
	RETURN 1
)[0]
LET b28=b27 || (
	FILTER b27 || HAS(D["dblp"]["thesis"],"entityProperty")
	RETURN 1
)[0]
LET b29=b28 || (
	FILTER b28 || HAS(D["dblp"]["thesis"],"cite")
	RETURN 1
)[0]
LET b30=b29 || (
	FILTER b29 || HAS(D["dblp"]["thesis"],"school")
	RETURN 1
)[0]
LET b31=b30 || (
	FILTER b30 || HAS(D["dblp"]["thesis"],"publisher")
	RETURN 1
)[0]
LET b32=b31 || (
	FILTER b31 || (
		let d0=b31 ? null : D["dblp"]["thesis"]["author"]
		FILTER IS_ARRAY(d0)
		FOR D1 in d0
		FILTER HAS(D1,"#text")
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b33=b32 || (
	FILTER b32 || (
		let d0=b32 ? null : D["dblp"]["thesis"]["editor"]
		FILTER IS_ARRAY(d0)
		FOR D1 in d0
		FILTER HAS(D1,"#text")
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b34=b33 || (
	FILTER b33 || HAS(D["dblp"]["thesis"]["publisher"],"#text")
	RETURN 1
)[0]
LET b35=b34 || (
	FILTER b34 || (
		let d0=b34 ? null : D["dblp"]["thesis"]["cite"]
		FILTER IS_ARRAY(d0)
		FOR D1 in d0
		FILTER HAS(D1,"#text")
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b36=b35 || (
	FILTER b35 || HAS(D["dblp"]["publication"],"author")
	RETURN 1
)[0]
LET b37=b36 || (
	FILTER b36 || HAS(D["dblp"]["publication"],"journal")
	RETURN 1
)[0]
LET b38=b37 || (
	FILTER b37 || HAS(D["dblp"]["publication"],"editor")
	RETURN 1
)[0]
LET b39=b38 || (
	FILTER b38 || HAS(D["dblp"]["publication"],"creator")
	RETURN 1
)[0]
LET b40=b39 || (
	FILTER b39 || HAS(D["dblp"]["publication"],"entityProperty")
	RETURN 1
)[0]
LET b41=b40 || (
	FILTER b40 || HAS(D["dblp"]["publication"],"cite")
	RETURN 1
)[0]
LET b42=b41 || (
	FILTER b41 || HAS(D["dblp"]["publication"],"school")
	RETURN 1
)[0]
LET b43=b42 || (
	FILTER b42 || HAS(D["dblp"]["publication"],"publisher")
	RETURN 1
)[0]
LET b44=b43 || (
	FILTER b43 || (
		let d0=b43 ? null : D["dblp"]["publication"]["author"]
		FILTER IS_ARRAY(d0)
		FOR D1 in d0
		FILTER HAS(D1,"#text")
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b45=b44 || (
	FILTER b44 || (
		let d0=b44 ? null : D["dblp"]["publication"]["editor"]
		FILTER IS_ARRAY(d0)
		FOR D1 in d0
		FILTER HAS(D1,"#text")
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b46=b45 || (
	FILTER b45 || HAS(D["dblp"]["publication"]["publisher"],"#text")
	RETURN 1
)[0]
LET b47=b46 || (
	FILTER b46 || (
		let d0=b46 ? null : D["dblp"]["publication"]["cite"]
		FILTER IS_ARRAY(d0)
		FOR D1 in d0
		FILTER HAS(D1,"#text")
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b48=b47 || (
	FILTER b47 || HAS(D["dblp"]["document"],"author")
	RETURN 1
)[0]
LET b49=b48 || (
	FILTER b48 || HAS(D["dblp"]["document"],"journal")
	RETURN 1
)[0]
LET b50=b49 || (
	FILTER b49 || HAS(D["dblp"]["document"],"editor")
	RETURN 1
)[0]
LET b51=b50 || (
	FILTER b50 || HAS(D["dblp"]["document"],"creator")
	RETURN 1
)[0]
LET b52=b51 || (
	FILTER b51 || HAS(D["dblp"]["document"],"entityProperty")
	RETURN 1
)[0]
LET b53=b52 || (
	FILTER b52 || HAS(D["dblp"]["document"],"cite")
	RETURN 1
)[0]
LET b54=b53 || (
	FILTER b53 || HAS(D["dblp"]["document"],"school")
	RETURN 1
)[0]
LET b55=b54 || (
	FILTER b54 || HAS(D["dblp"]["document"],"publisher")
	RETURN 1
)[0]
LET b56=b55 || (
	FILTER b55 || (
		let d0=b55 ? null : D["dblp"]["document"]["author"]
		FILTER IS_ARRAY(d0)
		FOR D1 in d0
		FILTER HAS(D1,"#text")
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b57=b56 || (
	FILTER b56 || (
		let d0=b56 ? null : D["dblp"]["document"]["editor"]
		FILTER IS_ARRAY(d0)
		FOR D1 in d0
		FILTER HAS(D1,"#text")
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b58=b57 || (
	FILTER b57 || HAS(D["dblp"]["document"]["publisher"],"#text")
	RETURN 1
)[0]
LET b59=b58 || (
	FILTER b58 || (
		let d0=b58 ? null : D["dblp"]["document"]["cite"]
		FILTER IS_ARRAY(d0)
		FOR D1 in d0
		FILTER HAS(D1,"#text")
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b60=b59 || (
	FILTER b59 || HAS(D["dblp"]["mastersthesis"],"author")
	RETURN 1
)[0]
LET b61=b60 || (
	FILTER b60 || HAS(D["dblp"]["mastersthesis"],"journal")
	RETURN 1
)[0]
LET b62=b61 || (
	FILTER b61 || HAS(D["dblp"]["mastersthesis"],"editor")
	RETURN 1
)[0]
LET b63=b62 || (
	FILTER b62 || HAS(D["dblp"]["mastersthesis"],"creator")
	RETURN 1
)[0]
LET b64=b63 || (
	FILTER b63 || HAS(D["dblp"]["mastersthesis"],"entityProperty")
	RETURN 1
)[0]
LET b65=b64 || (
	FILTER b64 || HAS(D["dblp"]["mastersthesis"],"cite")
	RETURN 1
)[0]
LET b66=b65 || (
	FILTER b65 || HAS(D["dblp"]["mastersthesis"],"school")
	RETURN 1
)[0]
LET b67=b66 || (
	FILTER b66 || HAS(D["dblp"]["mastersthesis"],"publisher")
	RETURN 1
)[0]
LET b68=b67 || (
	FILTER b67 || (
		let d0=b67 ? null : D["dblp"]["mastersthesis"]["author"]
		FILTER IS_ARRAY(d0)
		FOR D1 in d0
		FILTER HAS(D1,"#text")
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b69=b68 || (
	FILTER b68 || (
		let d0=b68 ? null : D["dblp"]["mastersthesis"]["editor"]
		FILTER IS_ARRAY(d0)
		FOR D1 in d0
		FILTER HAS(D1,"#text")
		RETURN 1
	)[0]
	RETURN 1
)[0]
FILTER b69
return D