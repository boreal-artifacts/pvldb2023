FOR D in `tf_DBLP_DBLP-simplified-`
LET b0=(
	let d0=D["dblp"]["book"]["publisher"]["#text"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
LET b1=b0 || (
	let d0=b0 ? null : D["dblp"]["book"]["publisher"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
FILTER b1
return D