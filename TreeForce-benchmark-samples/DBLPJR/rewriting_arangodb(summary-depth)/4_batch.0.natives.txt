FOR D in `tf_DBLP_DBLP-simplified-`
LET b0=(
	let d0=D["dblp"]["proceedings"]["publisher"]["#text"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
LET b1=b0 || (
	let d0=b0 ? null : D["dblp"]["proceedings"]["publisher"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
LET b2=b1 || (
	let d0=b1 ? null : D["dblp"]["book"]["publisher"]["#text"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
LET b3=b2 || (
	let d0=b2 ? null : D["dblp"]["book"]["publisher"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
LET b4=b3 || (
	let d0=b3 ? null : D["dblp"]["incollection"]["publisher"]["#text"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
LET b5=b4 || (
	let d0=b4 ? null : D["dblp"]["incollection"]["publisher"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
LET b6=b5 || (
	let d0=b5 ? null : D["dblp"]["article"]["publisher"]["#text"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
LET b7=b6 || (
	let d0=b6 ? null : D["dblp"]["article"]["publisher"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
LET b8=b7 || (
	let d0=b7 ? null : D["dblp"]["phdthesis"]["publisher"]["#text"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
LET b9=b8 || (
	let d0=b8 ? null : D["dblp"]["phdthesis"]["publisher"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
LET b10=b9 || (
	let d0=b9 ? null : D["dblp"]["inproceedings"]["publisher"]["#text"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
LET b11=b10 || (
	let d0=b10 ? null : D["dblp"]["inproceedings"]["publisher"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
LET b12=b11 || (
	let d0=b11 ? null : D["dblp"]["mastersthesis"]["publisher"]["#text"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
LET b13=b12 || (
	let d0=b12 ? null : D["dblp"]["mastersthesis"]["publisher"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
LET b14=b13 || (
	let d0=b13 ? null : D["dblp"]["document"]["publisher"]["#text"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
LET b15=b14 || (
	let d0=b14 ? null : D["dblp"]["document"]["publisher"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
LET b16=b15 || (
	let d0=b15 ? null : D["dblp"]["thesis"]["publisher"]["#text"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
LET b17=b16 || (
	let d0=b16 ? null : D["dblp"]["thesis"]["publisher"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
LET b18=b17 || (
	let d0=b17 ? null : D["dblp"]["publication"]["publisher"]["#text"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
LET b19=b18 || (
	let d0=b18 ? null : D["dblp"]["publication"]["publisher"]
	FILTER d0=="Springer"
	RETURN 1
)[0]
FILTER b19
return D