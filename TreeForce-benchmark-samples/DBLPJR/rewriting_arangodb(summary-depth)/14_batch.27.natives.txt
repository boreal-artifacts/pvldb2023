FOR D in `tf_DBLP_DBLP-simplified-`
LET b0=(
	let d0=D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["cdrom"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["cite"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b1=b0 || (
	let d0=b0 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["url"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["cite"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b2=b1 || (
	let d0=b1 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["note"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["cite"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b3=b2 || (
	let d0=b2 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["volume"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["cite"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b4=b3 || (
	let d0=b3 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["isbn"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["cite"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b5=b4 || (
	let d0=b4 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["dataProperty"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["cite"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b6=b5 || (
	let d0=b5 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["number"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["cite"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b7=b6 || (
	let d0=b6 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["booktitle"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["cite"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b8=b7 || (
	let d0=b7 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["publnr"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["cite"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b9=b8 || (
	let d0=b8 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["month"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["cite"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b10=b9 || (
	let d0=b9 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["pages"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["cite"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b11=b10 || (
	let d0=b10 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["chapter"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["cite"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b12=b11 || (
	let d0=b11 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["series"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["cite"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b13=b12 || (
	let d0=b12 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["author"]
	FILTER d1=="Dana Randall"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["isbn"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="2006"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b14=b13 || (
	let d0=b13 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["publisher"]
	FILTER d1=="Dana Randall"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["isbn"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="2006"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b15=b14 || (
	let d0=b14 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["cite"]
	FILTER d1=="Dana Randall"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["isbn"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="2006"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b16=b15 || (
	let d0=b15 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["school"]
	FILTER d1=="Dana Randall"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["isbn"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="2006"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b17=b16 || (
	let d0=b16 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["journal"]
	FILTER d1=="Dana Randall"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["isbn"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="2006"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b18=b17 || (
	let d0=b17 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["editor"]
	FILTER d1=="Dana Randall"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["isbn"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="2006"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b19=b18 || (
	let d0=b18 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["entityProperty"]
	FILTER d1=="Dana Randall"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["isbn"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="2006"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b20=b19 || (
	let d0=b19 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["creator"]
	FILTER d1=="Dana Randall"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["isbn"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="2006"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b21=b20 || (
	let d0=b20 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	FILTER HAS(d0,"title")
	FILTER (
		let d1=d0["cite"]
		FILTER IS_ARRAY(d1)
		FOR D2 in d1
		let d3=D2["#text"]
		FILTER d3=="Dana Randall"
		RETURN 1
	)[0]
	FILTER (
		let d4=d0["url"]
		FILTER IS_ARRAY(d4)
		FOR D5 in d4
		let d6=D5["#text"]
		FILTER d6=="2006"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b22=b21 || (
	let d0=b21 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	FILTER HAS(d0,"title")
	FILTER (
		let d1=d0["cite"]
		FILTER IS_ARRAY(d1)
		FOR D2 in d1
		let d3=D2["#text"]
		FILTER d3=="Dana Randall"
		RETURN 1
	)[0]
	FILTER (
		let d4=d0["note"]
		FILTER IS_ARRAY(d4)
		FOR D5 in d4
		let d6=D5["#text"]
		FILTER d6=="2006"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b23=b22 || (
	let d0=b22 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	FILTER HAS(d0,"title")
	FILTER (
		let d1=d0["author"]
		FILTER IS_ARRAY(d1)
		FOR D2 in d1
		let d3=D2["#text"]
		FILTER d3=="Dana Randall"
		RETURN 1
	)[0]
	FILTER (
		let d4=d0["url"]
		FILTER IS_ARRAY(d4)
		FOR D5 in d4
		let d6=D5["#text"]
		FILTER d6=="2006"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b24=b23 || (
	let d0=b23 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	FILTER HAS(d0,"title")
	let d1=d0["publisher"]["#text"]
	FILTER d1=="Dana Randall"
	let d2=d0["series"]["#text"]
	FILTER d2=="2006"
	RETURN 1
)[0]
LET b25=b24 || (
	let d0=b24 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	FILTER HAS(d0,"title")
	FILTER (
		let d1=d0["editor"]
		FILTER IS_ARRAY(d1)
		FOR D2 in d1
		let d3=D2["#text"]
		FILTER d3=="Dana Randall"
		RETURN 1
	)[0]
	FILTER (
		let d4=d0["ee"]
		FILTER IS_ARRAY(d4)
		FOR D5 in d4
		let d6=D5["#text"]
		FILTER d6=="2006"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b26=b25 || (
	let d0=b25 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	FILTER HAS(d0,"title")
	FILTER (
		let d1=d0["editor"]
		FILTER IS_ARRAY(d1)
		FOR D2 in d1
		let d3=D2["#text"]
		FILTER d3=="Dana Randall"
		RETURN 1
	)[0]
	let d4=d0["series"]["#text"]
	FILTER d4=="2006"
	RETURN 1
)[0]
LET b27=b26 || (
	let d0=b26 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["title"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["editor"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b28=b27 || (
	let d0=b27 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["year"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["editor"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b29=b28 || (
	let d0=b28 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["crossref"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["editor"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b30=b29 || (
	let d0=b29 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["ee"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["editor"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b31=b30 || (
	let d0=b30 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["cdrom"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["editor"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b32=b31 || (
	let d0=b31 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["url"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["editor"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b33=b32 || (
	let d0=b32 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["note"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["editor"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b34=b33 || (
	let d0=b33 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["volume"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["editor"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b35=b34 || (
	let d0=b34 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["isbn"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["editor"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b36=b35 || (
	let d0=b35 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["dataProperty"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["editor"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b37=b36 || (
	let d0=b36 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["number"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["editor"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b38=b37 || (
	let d0=b37 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["booktitle"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["editor"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b39=b38 || (
	let d0=b38 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["publnr"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["editor"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b40=b39 || (
	let d0=b39 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["month"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["editor"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b41=b40 || (
	let d0=b40 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["pages"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["editor"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b42=b41 || (
	let d0=b41 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["chapter"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["editor"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b43=b42 || (
	let d0=b42 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["series"]
	FILTER d1=="2006"
	FILTER HAS(d0,"title")
	FILTER (
		let d2=d0["editor"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="Dana Randall"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b44=b43 || (
	let d0=b43 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	FILTER HAS(d0,"title")
	FILTER (
		let d1=d0["cite"]
		FILTER IS_ARRAY(d1)
		FOR D2 in d1
		let d3=D2["#text"]
		FILTER d3=="Dana Randall"
		RETURN 1
	)[0]
	FILTER (
		let d4=d0["isbn"]
		FILTER IS_ARRAY(d4)
		FOR D5 in d4
		let d6=D5["#text"]
		FILTER d6=="2006"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b45=b44 || (
	let d0=b44 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	FILTER HAS(d0,"title")
	FILTER (
		let d1=d0["author"]
		FILTER IS_ARRAY(d1)
		FOR D2 in d1
		let d3=D2["#text"]
		FILTER d3=="Dana Randall"
		RETURN 1
	)[0]
	FILTER (
		let d4=d0["note"]
		FILTER IS_ARRAY(d4)
		FOR D5 in d4
		let d6=D5["#text"]
		FILTER d6=="2006"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b46=b45 || (
	let d0=b45 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	FILTER HAS(d0,"title")
	FILTER (
		let d1=d0["author"]
		FILTER IS_ARRAY(d1)
		FOR D2 in d1
		let d3=D2["#text"]
		FILTER d3=="Dana Randall"
		RETURN 1
	)[0]
	FILTER (
		let d4=d0["isbn"]
		FILTER IS_ARRAY(d4)
		FOR D5 in d4
		let d6=D5["#text"]
		FILTER d6=="2006"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b47=b46 || (
	let d0=b46 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	FILTER HAS(d0,"title")
	FILTER (
		let d1=d0["editor"]
		FILTER IS_ARRAY(d1)
		FOR D2 in d1
		let d3=D2["#text"]
		FILTER d3=="Dana Randall"
		RETURN 1
	)[0]
	FILTER (
		let d4=d0["url"]
		FILTER IS_ARRAY(d4)
		FOR D5 in d4
		let d6=D5["#text"]
		FILTER d6=="2006"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b48=b47 || (
	let d0=b47 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	FILTER HAS(d0,"title")
	let d1=d0["publisher"]["#text"]
	FILTER d1=="Dana Randall"
	FILTER (
		let d2=d0["url"]
		FILTER IS_ARRAY(d2)
		FOR D3 in d2
		let d4=D3["#text"]
		FILTER d4=="2006"
		RETURN 1
	)[0]
	RETURN 1
)[0]
LET b49=b48 || (
	let d0=b48 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["author"]
	FILTER d1=="Dana Randall"
	let d2=d0["title"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
LET b50=b49 || (
	let d0=b49 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["publisher"]
	FILTER d1=="Dana Randall"
	let d2=d0["title"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
LET b51=b50 || (
	let d0=b50 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["cite"]
	FILTER d1=="Dana Randall"
	let d2=d0["title"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
LET b52=b51 || (
	let d0=b51 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["school"]
	FILTER d1=="Dana Randall"
	let d2=d0["title"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
LET b53=b52 || (
	let d0=b52 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["journal"]
	FILTER d1=="Dana Randall"
	let d2=d0["title"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
LET b54=b53 || (
	let d0=b53 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["editor"]
	FILTER d1=="Dana Randall"
	let d2=d0["title"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
LET b55=b54 || (
	let d0=b54 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["entityProperty"]
	FILTER d1=="Dana Randall"
	let d2=d0["title"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
LET b56=b55 || (
	let d0=b55 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["creator"]
	FILTER d1=="Dana Randall"
	let d2=d0["title"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
LET b57=b56 || (
	let d0=b56 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["author"]
	FILTER d1=="Dana Randall"
	let d2=d0["year"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
LET b58=b57 || (
	let d0=b57 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["publisher"]
	FILTER d1=="Dana Randall"
	let d2=d0["year"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
LET b59=b58 || (
	let d0=b58 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["cite"]
	FILTER d1=="Dana Randall"
	let d2=d0["year"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
LET b60=b59 || (
	let d0=b59 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["school"]
	FILTER d1=="Dana Randall"
	let d2=d0["year"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
LET b61=b60 || (
	let d0=b60 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["journal"]
	FILTER d1=="Dana Randall"
	let d2=d0["year"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
LET b62=b61 || (
	let d0=b61 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["editor"]
	FILTER d1=="Dana Randall"
	let d2=d0["year"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
LET b63=b62 || (
	let d0=b62 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["entityProperty"]
	FILTER d1=="Dana Randall"
	let d2=d0["year"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
LET b64=b63 || (
	let d0=b63 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["creator"]
	FILTER d1=="Dana Randall"
	let d2=d0["year"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
LET b65=b64 || (
	let d0=b64 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["author"]
	FILTER d1=="Dana Randall"
	let d2=d0["crossref"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
LET b66=b65 || (
	let d0=b65 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["publisher"]
	FILTER d1=="Dana Randall"
	let d2=d0["crossref"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
LET b67=b66 || (
	let d0=b66 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["cite"]
	FILTER d1=="Dana Randall"
	let d2=d0["crossref"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
LET b68=b67 || (
	let d0=b67 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["school"]
	FILTER d1=="Dana Randall"
	let d2=d0["crossref"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
LET b69=b68 || (
	let d0=b68 ? null : D["dblp"]["phdthesis"]
	FILTER IS_OBJECT(d0)
	FILTER HAS(d0,"journal")
	let d1=d0["journal"]
	FILTER d1=="Dana Randall"
	let d2=d0["crossref"]
	FILTER d2=="2006"
	FILTER HAS(d0,"title")
	RETURN 1
)[0]
FILTER b69
return D