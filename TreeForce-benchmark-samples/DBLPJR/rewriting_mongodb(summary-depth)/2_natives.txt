{"dblp.incollection.series": {"$exists": true}}
{"dblp.incollection.dataProperty": {"$exists": true}}
{"dblp.incollection.publnr": {"$exists": true}}
{"dblp.incollection.number": {"$exists": true}}
{"dblp.incollection.volume": {"$exists": true}}
{"dblp.incollection.booktitle": {"$exists": true}}
{"dblp.incollection.pages": {"$exists": true}}
{"dblp.incollection.month": {"$exists": true}}
{"dblp.incollection.chapter": {"$exists": true}}
{"dblp.incollection.year": {"$exists": true}}
{"dblp.incollection.title": {"$exists": true}}
{"dblp.incollection.isbn": {"$exists": true}}
{"dblp.incollection.ee": {"$exists": true}}
{"dblp.incollection.crossref": {"$exists": true}}
{"dblp.incollection.cdrom": {"$exists": true}}
{"dblp.incollection.url": {"$exists": true}}
{"dblp.incollection.note": {"$exists": true}}
{"dblp.incollection.note": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.incollection.isbn": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.incollection.ee": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.incollection.url": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.incollection.series.#text": {"$exists": true}}
{"dblp.article.series": {"$exists": true}}
{"dblp.article.dataProperty": {"$exists": true}}
{"dblp.article.publnr": {"$exists": true}}
{"dblp.article.number": {"$exists": true}}
{"dblp.article.volume": {"$exists": true}}
{"dblp.article.booktitle": {"$exists": true}}
{"dblp.article.pages": {"$exists": true}}
{"dblp.article.month": {"$exists": true}}
{"dblp.article.chapter": {"$exists": true}}
{"dblp.article.year": {"$exists": true}}
{"dblp.article.title": {"$exists": true}}
{"dblp.article.isbn": {"$exists": true}}
{"dblp.article.ee": {"$exists": true}}
{"dblp.article.crossref": {"$exists": true}}
{"dblp.article.cdrom": {"$exists": true}}
{"dblp.article.url": {"$exists": true}}
{"dblp.article.note": {"$exists": true}}
{"dblp.article.note": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.article.isbn": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.article.ee": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.article.url": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.article.series.#text": {"$exists": true}}
{"dblp.book.series": {"$exists": true}}
{"dblp.book.dataProperty": {"$exists": true}}
{"dblp.book.publnr": {"$exists": true}}
{"dblp.book.number": {"$exists": true}}
{"dblp.book.volume": {"$exists": true}}
{"dblp.book.booktitle": {"$exists": true}}
{"dblp.book.pages": {"$exists": true}}
{"dblp.book.month": {"$exists": true}}
{"dblp.book.chapter": {"$exists": true}}
{"dblp.book.year": {"$exists": true}}
{"dblp.book.title": {"$exists": true}}
{"dblp.book.isbn": {"$exists": true}}
{"dblp.book.ee": {"$exists": true}}
{"dblp.book.crossref": {"$exists": true}}
{"dblp.book.cdrom": {"$exists": true}}
{"dblp.book.url": {"$exists": true}}
{"dblp.book.note": {"$exists": true}}
{"dblp.book.note": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.book.isbn": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.book.ee": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.book.url": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.book.series.#text": {"$exists": true}}
{"dblp.document.series": {"$exists": true}}
{"dblp.document.dataProperty": {"$exists": true}}
{"dblp.document.publnr": {"$exists": true}}
{"dblp.document.number": {"$exists": true}}
{"dblp.document.volume": {"$exists": true}}
{"dblp.document.booktitle": {"$exists": true}}
{"dblp.document.pages": {"$exists": true}}
{"dblp.document.month": {"$exists": true}}
{"dblp.document.chapter": {"$exists": true}}
{"dblp.document.year": {"$exists": true}}
{"dblp.document.title": {"$exists": true}}
{"dblp.document.isbn": {"$exists": true}}
{"dblp.document.ee": {"$exists": true}}
{"dblp.document.crossref": {"$exists": true}}
{"dblp.document.cdrom": {"$exists": true}}
{"dblp.document.url": {"$exists": true}}
{"dblp.document.note": {"$exists": true}}
{"dblp.document.note": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.document.isbn": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.document.ee": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.document.url": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.document.series.#text": {"$exists": true}}
{"dblp.proceedings.series": {"$exists": true}}
{"dblp.proceedings.dataProperty": {"$exists": true}}
{"dblp.proceedings.publnr": {"$exists": true}}
{"dblp.proceedings.number": {"$exists": true}}
{"dblp.proceedings.volume": {"$exists": true}}
{"dblp.proceedings.booktitle": {"$exists": true}}
{"dblp.proceedings.pages": {"$exists": true}}
{"dblp.proceedings.month": {"$exists": true}}
{"dblp.proceedings.chapter": {"$exists": true}}
{"dblp.proceedings.year": {"$exists": true}}
{"dblp.proceedings.title": {"$exists": true}}
{"dblp.proceedings.isbn": {"$exists": true}}
{"dblp.proceedings.ee": {"$exists": true}}
{"dblp.proceedings.crossref": {"$exists": true}}
{"dblp.proceedings.cdrom": {"$exists": true}}
{"dblp.proceedings.url": {"$exists": true}}
{"dblp.proceedings.note": {"$exists": true}}
{"dblp.proceedings.note": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.proceedings.isbn": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.proceedings.ee": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.proceedings.url": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.proceedings.series.#text": {"$exists": true}}
{"dblp.inproceedings.series": {"$exists": true}}
{"dblp.inproceedings.dataProperty": {"$exists": true}}
{"dblp.inproceedings.publnr": {"$exists": true}}
{"dblp.inproceedings.number": {"$exists": true}}
{"dblp.inproceedings.volume": {"$exists": true}}
{"dblp.inproceedings.booktitle": {"$exists": true}}
{"dblp.inproceedings.pages": {"$exists": true}}
{"dblp.inproceedings.month": {"$exists": true}}
{"dblp.inproceedings.chapter": {"$exists": true}}
{"dblp.inproceedings.year": {"$exists": true}}
{"dblp.inproceedings.title": {"$exists": true}}
{"dblp.inproceedings.isbn": {"$exists": true}}
{"dblp.inproceedings.ee": {"$exists": true}}
{"dblp.inproceedings.crossref": {"$exists": true}}
{"dblp.inproceedings.cdrom": {"$exists": true}}
{"dblp.inproceedings.url": {"$exists": true}}
{"dblp.inproceedings.note": {"$exists": true}}
{"dblp.inproceedings.note": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.inproceedings.isbn": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.inproceedings.ee": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.inproceedings.url": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.inproceedings.series.#text": {"$exists": true}}
{"dblp.phdthesis.series": {"$exists": true}}
{"dblp.phdthesis.dataProperty": {"$exists": true}}
{"dblp.phdthesis.publnr": {"$exists": true}}
{"dblp.phdthesis.number": {"$exists": true}}
{"dblp.phdthesis.volume": {"$exists": true}}
{"dblp.phdthesis.booktitle": {"$exists": true}}
{"dblp.phdthesis.pages": {"$exists": true}}
{"dblp.phdthesis.month": {"$exists": true}}
{"dblp.phdthesis.chapter": {"$exists": true}}
{"dblp.phdthesis.year": {"$exists": true}}
{"dblp.phdthesis.title": {"$exists": true}}
{"dblp.phdthesis.isbn": {"$exists": true}}
{"dblp.phdthesis.ee": {"$exists": true}}
{"dblp.phdthesis.crossref": {"$exists": true}}
{"dblp.phdthesis.cdrom": {"$exists": true}}
{"dblp.phdthesis.url": {"$exists": true}}
{"dblp.phdthesis.note": {"$exists": true}}
{"dblp.phdthesis.note": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.phdthesis.isbn": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.phdthesis.ee": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.phdthesis.url": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.phdthesis.series.#text": {"$exists": true}}
{"dblp.publication.series": {"$exists": true}}
{"dblp.publication.dataProperty": {"$exists": true}}
{"dblp.publication.publnr": {"$exists": true}}
{"dblp.publication.number": {"$exists": true}}
{"dblp.publication.volume": {"$exists": true}}
{"dblp.publication.booktitle": {"$exists": true}}
{"dblp.publication.pages": {"$exists": true}}
{"dblp.publication.month": {"$exists": true}}
{"dblp.publication.chapter": {"$exists": true}}
{"dblp.publication.year": {"$exists": true}}
{"dblp.publication.title": {"$exists": true}}
{"dblp.publication.isbn": {"$exists": true}}
{"dblp.publication.ee": {"$exists": true}}
{"dblp.publication.crossref": {"$exists": true}}
{"dblp.publication.cdrom": {"$exists": true}}
{"dblp.publication.url": {"$exists": true}}
{"dblp.publication.note": {"$exists": true}}
{"dblp.publication.note": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.publication.isbn": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.publication.ee": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.publication.url": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.publication.series.#text": {"$exists": true}}
{"dblp.thesis.series": {"$exists": true}}
{"dblp.thesis.dataProperty": {"$exists": true}}
{"dblp.thesis.publnr": {"$exists": true}}
{"dblp.thesis.number": {"$exists": true}}
{"dblp.thesis.volume": {"$exists": true}}
{"dblp.thesis.booktitle": {"$exists": true}}
{"dblp.thesis.pages": {"$exists": true}}
{"dblp.thesis.month": {"$exists": true}}
{"dblp.thesis.chapter": {"$exists": true}}
{"dblp.thesis.year": {"$exists": true}}
{"dblp.thesis.title": {"$exists": true}}
{"dblp.thesis.isbn": {"$exists": true}}
{"dblp.thesis.ee": {"$exists": true}}
{"dblp.thesis.crossref": {"$exists": true}}
{"dblp.thesis.cdrom": {"$exists": true}}
{"dblp.thesis.url": {"$exists": true}}
{"dblp.thesis.note": {"$exists": true}}
{"dblp.thesis.note": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.thesis.isbn": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.thesis.ee": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.thesis.url": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.thesis.series.#text": {"$exists": true}}
{"dblp.mastersthesis.series": {"$exists": true}}
{"dblp.mastersthesis.dataProperty": {"$exists": true}}
{"dblp.mastersthesis.publnr": {"$exists": true}}
{"dblp.mastersthesis.number": {"$exists": true}}
{"dblp.mastersthesis.volume": {"$exists": true}}
{"dblp.mastersthesis.booktitle": {"$exists": true}}
{"dblp.mastersthesis.pages": {"$exists": true}}
{"dblp.mastersthesis.month": {"$exists": true}}
{"dblp.mastersthesis.chapter": {"$exists": true}}
{"dblp.mastersthesis.year": {"$exists": true}}
{"dblp.mastersthesis.title": {"$exists": true}}
{"dblp.mastersthesis.isbn": {"$exists": true}}
{"dblp.mastersthesis.ee": {"$exists": true}}
{"dblp.mastersthesis.crossref": {"$exists": true}}
{"dblp.mastersthesis.cdrom": {"$exists": true}}
{"dblp.mastersthesis.url": {"$exists": true}}
{"dblp.mastersthesis.note": {"$exists": true}}
{"dblp.mastersthesis.note": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.mastersthesis.isbn": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.mastersthesis.ee": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.mastersthesis.url": {"$elemMatch": {"#text": {"$exists": true}}}}
{"dblp.mastersthesis.series.#text": {"$exists": true}}
