#!/bin/bash

# This file is used by the main script doTheTest.sh

JAVA_OPT="-Xmx100G -Xms10G"
JAVA_JAR="testRulewerk.jar"

dataSet="$1"
shift

while [[ 0 < $# ]]; do
	ruleFile=$1
	shift
	ruleName=$(basename "$ruleFile" .dlog)
	testDir="${dataSet}_$ruleName"
	confFile="$testDir/conf"
	date=$(date)
	resultFile="$testDir/result_$date.txt"
	
	cmd="java -jar '$JAVA_JAR' '$confFile' '$ruleFile' $JAVA_OPT"
	
	echo "# $cmd"
	echo "# writing $resultFile"
	
	(echo "# $cmd" ; eval "$cmd") > "$resultFile"
done
