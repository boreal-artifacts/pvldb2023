#!/bin/bash

jvm=10G

#params as follows
jar=$1
datacsv=$2
conf=$3
queryrules=$4
maxRepetition=$5

repetition=0
while (( repetition < $maxRepetition  ))
do 
	echo ""
      	echo "#### round ##### " 
      	echo ""

      	echo $confabsolute

	echo java -Xms${jvm} -Xmx${jvm} -jar $jar rulewerk-mat $conf  $queryrules
      
	repetition=$repetition+1
done
echo "rulewerk-mat done"
