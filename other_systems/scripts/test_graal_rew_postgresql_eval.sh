
# needs postgresql running

#param as follows
jvm=10G
jvmencode=100G
jar=$1
datacsv=$2
conf=$3
ucqfolder=$4
maxRepetition=$5
confEncoded=$6
dumb=temp/Q.dlgp

echo ""
echo "encode and load"
echo ""


#encode
echo java -Xms${jvmencode} -Xmx${jvmencode} -jar $jar integraal-encoding    $conf $dumb

#load
echo java -Xms${jvm} -Xmx${jvm} -jar $jar loadcsv-rdbms-encoded $confEncoded $dumb

#query
repetition=0
while (( repetition < $maxRepetition ))
do
        echo "#### round ##### " 
        echo ""

        for ucq in ${ucqfolder}/*
        do
        echo        java -Xms${jvm} -Xmx${jvm} -jar $jar integraal-rdbms $conf  $ucq
        done

        repetition=$repetition+1
done
echo "done"
