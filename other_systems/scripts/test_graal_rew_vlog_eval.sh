#!/bin/bash

#param as follows
jvm=10G
jar=$1
datacsv=$2
conf=$3
ucqfolder=$4
maxRepetition=$5

repetition=0
while (( $repetition < $maxRepetition ))
do 
      	echo ""
	echo "#### round ##### " 
      	echo ""

      	echo $confabsolute

      	for ucq in ${ucqfolder}/*
	do
	echo	 java -Xms${jvm} -Xmx${jvm} -jar $jar rulewerk-rew $conf  $ucq
 	done
      
	repetition=$repetition+1
done
echo "rulewerk-rew done"

