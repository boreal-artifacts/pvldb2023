#! /bin/bash

# This file is used by the main script doTheTest.sh

jar=hi-rule-0.1-SNAPSHOT.jar
wd=$(pwd)
configFile=".1_export_tmp"

while [[ 0 < $# ]] ; do
  file="$1"
  shift
  fname=$(basename "$file" ".json")
  
  cd data/

config=$(cat << EOD
base.path=$wd/data
export.trees.input.file=\${base.path}/$fname.json
export.format=csv
output.measures=std://out
export.trees.pattern.default=$fname/csv/%1\$s.%2\$s
EOD
)
  echo "$config" > $configFile

  cmd="java -jar '../$jar' export -c $configFile"
  eval "$cmd"
  rm "$exportDir/_id.csv"
  cd ../
  
  bash scripts/csv_makeconf.sh "data/$fname/csv" &&
  bash "data/$fname/conf.sh" "data/$fname/csv" > "data/$fname/conf"
done
