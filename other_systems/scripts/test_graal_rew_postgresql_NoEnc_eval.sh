#!/bin/bash


#need postgresql running

#param as follows
jvm=10G
jar=$1
datacsv=$2
conf=$3
ucqfolder=$4
maxRepetition=$5
dumb=temp/Q.dlgp

echo ""
echo "load"

#load
echo java -Xms${jvm} -Xmx${jvm} -jar $jar loadcsv-rdbms $conf $dumb

#query
repetition=0
while (( $repetition < $maxRepetition ))
do
        echo "#### round ##### " 
        echo ""

        for ucq in ${ucqfolder}/*
        do
        echo         java -Xms${jvm} -Xmx${jvm} -jar $jar integraal-rdbms $conf  $ucq
        done

        repetition=$repetition+1
done
echo "done"
