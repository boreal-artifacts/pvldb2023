

#!/bin/bash

jar=integraal.jar
maxRepetition=1

# set the folder
folder=benchmarks/dblp #change to GithHub or to a specific XMark folder

datacsv=${folder}/export_csv
conf=${folder}/conf
confEnc=${folder}/conf_enc
queries=${folder}/queries
rules=${folder}/rules
rewritings=${folder}/rewritings

# make sur postgresql is running
# comment echo commands within scripts when ready to launch the test

#Rulewerk/VLog mat
./scripts/test_vlog_mat.sh $jar $datacsv $conf ${rules}/vlog/query_rules.dlgp $maxRepetition

#Graal rew + Rulewerk/VLog eval
./scripts/test_graal_rew_vlog_eval.sh $jar $datacsv $conf ${rewritings}/vlog $maxRepetition

#Graal rew + Postgresql-NoEnc eval
./scripts/test_graal_rew_postgresql_NoEnc_eval.sh $jar $datacsv $conf $rewritings/integraal $maxRepetition

#Graal rew + Postgresql eval
./scripts/test_graal_rew_postgresql_eval.sh $jar $datacsv $conf $rewritings/integraal $maxRepetition $confEnc 




